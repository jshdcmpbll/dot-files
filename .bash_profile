parse_git_branch() {
       git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/'
}
export PS1="\w "
export CLICOLOR=1
export export LSCOLORS=gxBxhxDxfxhxhxhxhxcxcx 
source ~/.alias
if [ -e /home/jsh/.nix-profile/etc/profile.d/nix.sh ]; then . /home/jsh/.nix-profile/etc/profile.d/nix.sh; fi # added by Nix installer
